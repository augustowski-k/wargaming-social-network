namespace Domain.Enums
{
    public enum ReactionType
    {
        Happy = 1,
        Sad = 2,
        Angry = 3,
        Shocked = 4
    }
}