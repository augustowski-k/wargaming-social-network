namespace Domain.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;
    
    public interface IStatusRepository: IRepository
    {
        Task Add(Status status);
        
        Task<List<Status>> GetAllOrderedByDateDescending();
    }
}