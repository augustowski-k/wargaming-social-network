using System;
using Domain.Auth;

namespace Domain.Models 
{
    public class Comment: IEntity
    {
        public Guid Id { get; set; }
        
        public Guid StatusId { get; set; }
        
        public AppUser Author { get; set; }
        
        public DateTime Date { get; set; }

        public string Content { get; set; }
    }
}