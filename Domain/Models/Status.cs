namespace Domain.Models
{
    using System;
    using System.Collections.Generic;
    using Auth;
    
    public class Status: IEntity
    {
        public Guid Id { get; set; }
        
        public AppUser Author { get; set; }

        public DateTime Date { get; set; }

        public string Content { get; set; }

        public List<Reaction> Reactions { get; set; }

        public List<Comment> Comments { get; set; }
    }
}