namespace Domain.Models
{
    using System;
    using Auth;
    using Enums;
    
    public class Reaction: IEntity
    {
        public Guid Id { get; set; }

        public AppUser User { get; set; }
        
        public Guid StatusId { get; set; }

        public ReactionType ReactionType { get; set; }
    }
}