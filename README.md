# wargaming social network

You can run this project by cloning the repository and executing following commands:

```
docker-compose build
docker-compose up
```

This sequence will build application and create sql server image with newest entity framework migration.

To achieve this behavior I modified and used code from official sql server docker repository:  
https://github.com/microsoft/mssql-docker/tree/master/linux/preview/examples/mssql-customize