FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine AS build

WORKDIR /build
    
COPY WargamingSocialNetwork.sln .
COPY Web/Web.csproj Web/
COPY Infrastructure/Infrastructure.csproj Infrastructure/
COPY Domain/Domain.csproj Domain/
COPY Application/Application.csproj Application/

RUN dotnet restore

COPY . .

ENV APP_DB_CONNSTRING "Data Source=.;Initial Catalog=dummy_db;Integrated Security=True"

RUN dotnet ef migrations script -p Infrastructure/Infrastructure.csproj -s Web/Web.csproj -i -o /output/db.sql


FROM mcr.microsoft.com/mssql/server:2017-latest

ARG dbName=wargaming_db
ENV APP_DB_NAME=$dbName

RUN mkdir -p /usr/config
WORKDIR /usr/config

COPY DbDocker /usr/config

COPY --from=build /output/db.sql /usr/config/setup.sql

RUN chmod +x /usr/config/entrypoint.sh
RUN chmod +x /usr/config/configure-db.sh

ENTRYPOINT ["./entrypoint.sh"]