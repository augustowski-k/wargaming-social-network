namespace Web.ViewModels.Home
{
    using System.Collections.Generic;
    using Domain.Models;

    public class HomeIndexViewModel
    {
        public List<Status> Statuses { get; set; }
    }
}