namespace Web.Controllers
{
    using System.Threading.Tasks;
    using Application.Services;
    using Domain.Auth;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using ViewModels.Status;

    public class StatusController : Controller
    {
        private readonly IStatusService _statusService;
        private readonly UserManager<AppUser> _userManager;

        public StatusController(
            IStatusService statusService,
            UserManager<AppUser> userManager)
        {
            _statusService = statusService;
            _userManager = userManager;
        }
        
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddStatus([FromForm]CreateStatusViewModel viewModel)
        {
            AppUser currentUser = await _userManager.GetUserAsync(User);

            await _statusService.CreateStatus(viewModel.Status, currentUser);

            return RedirectToAction("Index");
        }
    }
}