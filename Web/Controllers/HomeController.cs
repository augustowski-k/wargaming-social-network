﻿namespace Web.Controllers
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using Application.Services;
    using Domain.Models;
    using Microsoft.AspNetCore.Mvc;
    using Models;
    using Microsoft.AspNetCore.Authorization;
    using ViewModels.Home;

    public class HomeController : Controller
    {
        private readonly IStatusService _statusService;

        public HomeController(IStatusService statusService)
        {
            _statusService = statusService;
        }
        
        public async Task<IActionResult> Index()
        {
            List<Status> statuses = await _statusService.GetAllChronological();
            HomeIndexViewModel viewModel = new HomeIndexViewModel{Statuses = statuses};
            
            return View(viewModel);
        }
    
        [Authorize]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
