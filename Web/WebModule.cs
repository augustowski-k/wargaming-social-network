namespace Web
{
    using Domain.Repositories;
    using Domain.Auth;
    using Infrastructure.Auth;
    using System.Reflection;
    using Autofac;
    using Application;
    using Infrastructure;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.EntityFrameworkCore;
    using Module = Autofac.Module;

    public class WebModule: Module
    {
        private readonly Assembly[] _assemblies = {
            typeof(WebModule).Assembly,
            typeof(IRepository).Assembly,
            typeof(IService).Assembly,
            typeof(AppDbContext).Assembly
        };

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(
                    c => new AppDbContextFactory().CreateDbContext())
                .As<AppDbContext>()
                .As<DbContext>()
                .InstancePerLifetimeScope();

            builder.RegisterType<AppDbContextFactory>()
                .As<IAppDbContextFactory>();

            builder.RegisterType<AppUserStore>()
                .As<IUserStore<AppUser>>();

            builder.RegisterType<UserManager<AppUser>>();
            
            builder.RegisterAssemblyTypes(_assemblies)
                .Where(t => t.IsAssignableTo<IRepository>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(_assemblies)
                .Where(t => t.IsAssignableTo<IService>())
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}