namespace Application.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Domain.Auth;
    using Domain.Models;

    public interface IStatusService: IService
    {
        Task CreateStatus(string status, AppUser author);
        
        Task<List<Status>> GetAllChronological();
    }
}