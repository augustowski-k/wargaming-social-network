namespace Application.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Domain.Auth;
    using Domain.Models;
    using Domain.Repositories;

    public class StatusService: IStatusService
    {
        private readonly IStatusRepository _statusRepository;

        public StatusService(IStatusRepository statusRepository)
        {
            _statusRepository = statusRepository;
        }

        public async Task CreateStatus(string status, AppUser author)
        {
            Status statusEntity = new Status
            {
                Content = status,
                Author = author,
                Date = DateTime.Now
            };

            await _statusRepository.Add(statusEntity);
        }

        public async Task<List<Status>> GetAllChronological()
        {
            return await _statusRepository.GetAllOrderedByDateDescending();
        }
    }
}