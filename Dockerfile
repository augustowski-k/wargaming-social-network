FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine AS build

WORKDIR /build
    
COPY WargamingSocialNetwork.sln .
COPY Web/Web.csproj Web/
COPY Infrastructure/Infrastructure.csproj Infrastructure/
COPY Domain/Domain.csproj Domain/
COPY Application/Application.csproj Application/

RUN dotnet restore

COPY . .

RUN dotnet publish -c Release -o /output


FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine AS runtime

WORKDIR /app

COPY --from=build /output .

CMD ["dotnet", "Web.dll"]