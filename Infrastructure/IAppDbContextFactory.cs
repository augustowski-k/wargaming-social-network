namespace Infrastructure
{
    using Microsoft.EntityFrameworkCore.Design;
    
    public interface IAppDbContextFactory: IDesignTimeDbContextFactory<AppDbContext>
    {
        AppDbContext CreateDbContext();
    }
}