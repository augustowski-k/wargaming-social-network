using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastructure
{
    public class AppDbContextFactory: IAppDbContextFactory
    {
        public AppDbContext CreateDbContext()
        {
            return CreateDbContext(null);
        }
        
        public AppDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            
            optionsBuilder.UseSqlServer(
                Environment.GetEnvironmentVariable("APP_DB_CONNSTRING") 
                ?? throw new ApplicationException("Environment variable with db connection string was not specified"));
            
            return new AppDbContext(optionsBuilder.Options);
        }
    }
}