namespace Infrastructure.Auth
{
    using System;
    using Domain.Auth;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    
    public class AppUserStore: UserStore<AppUser, AppRole, AppDbContext, Guid>
    {
        public AppUserStore(AppDbContext context, IdentityErrorDescriber describer = null) : base(context, describer)
        {
        }
    }
}