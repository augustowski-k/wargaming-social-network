namespace Infrastructure.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Domain.Models;
    using Domain.Repositories;
    using Microsoft.EntityFrameworkCore;
    
    public class SqlStatusRepository: IStatusRepository
    {
        private readonly IAppDbContextFactory _dbContextFactory;

        public SqlStatusRepository(IAppDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task<List<Status>> GetAllOrderedByDateDescending()
        {
            using (var dbContext = _dbContextFactory.CreateDbContext())
            {
                return await dbContext.Statuses
                    .Include(s => s.Author)
                    .OrderByDescending(s => s.Date)
                    .ToListAsync();
            }
        }

        public async Task Add(Status status)
        {
            using (var dbContext = _dbContextFactory.CreateDbContext())
            {
                dbContext.Add(status);
                dbContext.Attach(status.Author);

                await dbContext.SaveChangesAsync();
            }
        }
    }
}